package com.thalesgroup.flight_plans;

import java.time.LocalDateTime;

public class FlightPlan {
	private String callsign;
	private String waypoint;
	private LocalDateTime estimatedTimeOverflight;
	private Integer cflFeet;
	private Boolean airworkIntent;
	private String departureAerodrome;
	private String arrivalAerodrome;
	private String flightState;

	public FlightPlan(String callsign, String waypoint, LocalDateTime estimatedTimeOverflight, Integer cflFeet,
			Boolean airworkIntent, String departureAerodrome, String arrivalAerodrome, String flightState) {
		this.callsign = callsign;
		this.waypoint = waypoint;
		this.estimatedTimeOverflight = estimatedTimeOverflight;
		this.cflFeet = cflFeet;
		this.airworkIntent = airworkIntent;
		this.departureAerodrome = departureAerodrome;
		this.arrivalAerodrome = arrivalAerodrome;
		this.flightState = flightState;
	}

	public String getCallsign() {
		return callsign;
	}

	public void setCallsign(String callsign) {
		this.callsign = callsign;
	}

	public String getWaypoint() {
		return waypoint;
	}

	public void setWaypoint(String waypoint) {
		this.waypoint = waypoint;
	}

	public LocalDateTime getEstimatedTimeOverflight() {
		return estimatedTimeOverflight;
	}

	public void setEstimatedTimeOverflight(LocalDateTime estimatedTimeOverflight) {
		this.estimatedTimeOverflight = estimatedTimeOverflight;
	}

	public Integer getCflFeet() {
		return cflFeet;
	}

	public void setCflFeet(Integer cflFeet) {
		this.cflFeet = cflFeet;
	}

	public Boolean getAirworkIntent() {
		return airworkIntent;
	}

	public void setAirworkIntent(Boolean airworkIntent) {
		this.airworkIntent = airworkIntent;
	}

	public String getDepartureAerodrome() {
		return departureAerodrome;
	}

	public void setDepartureAerodrome(String departureAerodrome) {
		this.departureAerodrome = departureAerodrome;
	}

	public String getArrivalAerodrome() {
		return arrivalAerodrome;
	}

	public void setArrivalAerodrome(String arrivalAerodrome) {
		this.arrivalAerodrome = arrivalAerodrome;
	}

	public String getFlightState() {
		return flightState;
	}

	public void setFlightState(String flightState) {
		this.flightState = flightState;
	}
}
