package com.thalesgroup.flight_plans;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.ResourceBundle;
import java.util.Scanner;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

public class Controller implements Initializable {
	static final String INPUT_PATHNAME = "C:\\Users\\justin\\Desktop\\input.txt";
	static final String OUTPUT_PATHNAME = "C:\\Users\\justin\\Desktop\\output.txt";
	static final Integer CELL_HEIGHT = 24;
	static final Integer DISPLAYABLE_ROWS = 15;

	static final DateTimeFormatter estimatedTimeOverflightDisplayFormatter = DateTimeFormatter.ofPattern("HHmm");
	static final DateTimeFormatter estimatedTimeOverflightFileFormatter = DateTimeFormatter
			.ofPattern("yyyyMMdd:HHmmss");

	private double xOffset = 0;
	private double yOffset = 0;

	private ObservableList<FlightPlan> flightPlans;
	private FilteredList<FlightPlan> filterFlightPlans;
	private SortedList<FlightPlan> sortedFlightPlans;

	@FXML
	private Label label;
	@FXML
	private Button closeButton;

	@FXML
	private TableView<FlightPlan> tableView;

	@FXML
	private TableColumn<FlightPlan, String> callsignColumn;
	@FXML
	private TableColumn<FlightPlan, LocalDateTime> estimatedTimeOverflightColumn;
	@FXML
	private TableColumn<FlightPlan, Integer> cflFeetColumn;
	@FXML
	private TableColumn<FlightPlan, Boolean> airworkIntentColumn;
	@FXML
	private TableColumn<FlightPlan, String> departureAerodromeColumn;
	@FXML
	private TableColumn<FlightPlan, String> arrivalAerodromeColumn;

	@FXML
	private CheckMenuItem departureAerodromeCheckMenuItem;
	@FXML
	private CheckMenuItem arrivalAerodromeCheckMenuItem;

	@Override
	public void initialize(URL url, ResourceBundle resourceBundle) {
		label.setOnMousePressed(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				xOffset = event.getSceneX();
				yOffset = event.getSceneY();
			}
		});
		label.setOnMouseDragged(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				Stage stage = (Stage) label.getScene().getWindow();
				stage.setX(event.getScreenX() - xOffset);
				stage.setY(event.getScreenY() - yOffset);
			}
		});

		callsignColumn.setCellValueFactory(new PropertyValueFactory<>("callsign"));
		estimatedTimeOverflightColumn.setCellValueFactory(new PropertyValueFactory<>("estimatedTimeOverflight"));
		cflFeetColumn.setCellValueFactory(new PropertyValueFactory<>("cflFeet"));
		airworkIntentColumn.setCellValueFactory(new PropertyValueFactory<>("airworkIntent"));
		departureAerodromeColumn.setCellValueFactory(new PropertyValueFactory<>("departureAerodrome"));
		arrivalAerodromeColumn.setCellValueFactory(new PropertyValueFactory<>("arrivalAerodrome"));

		estimatedTimeOverflightColumn.setCellFactory(col -> new TableCell<FlightPlan, LocalDateTime>() {
			@Override
			protected void updateItem(LocalDateTime item, boolean empty) {
				super.updateItem(item, empty);
				if (empty)
					setText(null);
				else
					setText(String.format(item.format(estimatedTimeOverflightDisplayFormatter)));
			}
		});
		cflFeetColumn.setCellFactory(col -> new TableCell<FlightPlan, Integer>() {
			@Override
			protected void updateItem(Integer item, boolean empty) {
				super.updateItem(item, empty);
				if (empty)
					setText(null);
				else
					setText(Integer.toString(item / 100));
			}
		});

		final Image airworkIntentImage = new Image("com/thalesgroup/flight_plans/airworkIntent.png");
		airworkIntentColumn.setCellFactory(col -> new TableCell<FlightPlan, Boolean>() {
			private final ImageView imageView = new ImageView();

			{
				setGraphic(imageView);
			}

			@Override
			protected void updateItem(Boolean item, boolean empty) {
				if (empty || item == null) {
					imageView.setImage(null);
				} else {
					imageView.setImage(item ? airworkIntentImage : null);
				}
			}
		});

		try {
			flightPlans = loadFlightPlans(INPUT_PATHNAME);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		filterFlightPlans = new FilteredList<>(flightPlans,
				f -> f.getWaypoint().equals("FILET") || f.getWaypoint().equals("HUSKY"));
		sortedFlightPlans = new SortedList<>(filterFlightPlans);
		sortedFlightPlans.comparatorProperty().bind(tableView.comparatorProperty());

		tableView.setItems(sortedFlightPlans);
		tableView.getSortOrder().addAll(estimatedTimeOverflightColumn, callsignColumn);
		tableView.setMaxHeight(CELL_HEIGHT * (DISPLAYABLE_ROWS + 1));
	}

	public void close() {
		Stage stage = (Stage) closeButton.getScene().getWindow();
		stage.close();
	}

	public void archive() throws IOException {
		saveFlightPlans(OUTPUT_PATHNAME, flightPlans);
	}

	public void updateColumns() {
		departureAerodromeColumn.setVisible(departureAerodromeCheckMenuItem.isSelected());
		arrivalAerodromeColumn.setVisible(arrivalAerodromeCheckMenuItem.isSelected());
	}

	public static ObservableList<FlightPlan> loadFlightPlans(String pathname) throws FileNotFoundException {
		ObservableList<FlightPlan> records = FXCollections.observableArrayList();
		Scanner scanner = new Scanner(new File(pathname));
		while (scanner.hasNextLine()) {
			String line = scanner.nextLine();
			if (line.charAt(0) == '#') {
				continue;
			}
			ArrayList<String> fields = new ArrayList<>(Arrays.asList(line.split(", |,")));
			FlightPlan record = new FlightPlan(fields.get(0), fields.get(1),
					LocalDateTime.parse(fields.get(2), estimatedTimeOverflightFileFormatter),
					Integer.parseInt(fields.get(3)), fields.get(4).equals("1"), fields.get(5), fields.get(6),
					fields.get(7));
			records.add(record);
		}
		return records;
	}

	public static void saveFlightPlans(String pathname, ObservableList<FlightPlan> flightPlans) throws IOException {
		ArrayList<String> records = new ArrayList<>();
		records.add("#Column Key");
		records.add(
				"#Callsign, Waypoint, Estimated Time Overflight, CFL(feet), Airwork Intent (boolean), Departure Ae:qrodrome, Arrival Aerodrome, Flight State");
		flightPlans.forEach(r -> records.add(r.getCallsign() + ", " + r.getWaypoint() + ", "
				+ estimatedTimeOverflightFileFormatter.format(r.getEstimatedTimeOverflight()) + ", " + r.getCflFeet()
				+ "," + (r.getAirworkIntent() ? "1" : "0") + "," + r.getDepartureAerodrome() + ", "
				+ r.getArrivalAerodrome() + ", " + r.getFlightState()));
		Path file = Paths.get(pathname);
		Files.write(file, records, StandardCharsets.UTF_8);
	}
}
